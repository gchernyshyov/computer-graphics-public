import subprocess


def subtiles_join_mp4(input_mp4_file, input_srt_file, output_file):
    ffmpeg_command = ['ffmpeg', '-i', input_mp4_file, '-vf', 'subtitles=' + input_srt_file, output_file]
    subprocess.run(ffmpeg_command)


def subtitledmp4_join_mp3(input_mp4_file, input_mp3_file, output_file):
    ffmpeg_command = ['ffmpeg', '-i', input_mp4_file, '-i', input_mp3_file, '-c:v', 'copy', '-c:a', 'copy', output_file]
    subprocess.run(ffmpeg_command)


def changescale(input_mp4_file, output_mp4_file):
    ffmpeg_command = ['ffmpeg', '-i', input_mp4_file, '-vf', 'scale=1280:720', output_mp4_file]
    subprocess.run(ffmpeg_command)


subtiles_join_mp4('3.mp4', '1.srt', 'subtitledmp4.mp4')
subtitledmp4_join_mp3('subtitledmp4.mp4', '4.mp3', 'end.mp4')
changescale('end.mp4', 'final.mp4')
